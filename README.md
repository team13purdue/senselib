# SenseLib

Motion sensor assisted GPS location library for Android.

## NOTICE

**SenseLib has been moved to [GitHub](https://github.com/kevinxucs/senselib). Further development will continue there.**

**Code here will not be updated anymore.**

## Requirement

Android 2.3.3 (API 10)

## License

MIT License